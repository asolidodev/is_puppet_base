### Purpose ########
# This class provides a nagios plugin that can be used trigger and monitor MYSQL db test restores
define is_puppet_base::sysmon_mysqldump_health ( $mysqluser, $mysqlpassword, $dirs) {

  $bindir   = lookup('filesystem::bindir')
  $script_prefix = 'check-mysqldump-health'
  $scriptname = "${script_prefix}.sh"

  file { "${bindir}/${scriptname}":
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template("is_puppet_base/sysmon/${script_prefix}.sh.erb"),
    require => File[ $bindir ],
  }

}

