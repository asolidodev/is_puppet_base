### Purpose ########
# This class provides zimbra backups
class is_puppet_base::zimbra_backup (
  # Feature flag to turn on running the backup command with nice
  Boolean         $nice_enabled = false,
  # Nice has default of 10 but we use 15 here so that the backup is less
  # prioritary than other commands running with nice. The requirement
  # for the nice value to be an integer from -20 to 19 is from the nice command.
  Integer[-20,19] $nice_value = 15,
)
{

  $mysystype = 'zimbra'
  $backdir = lookup("${mysystype}::backdir")
  $ndays   = lookup("${mysystype}::backdays")
  $hour    = lookup("${mysystype}::backhour")
  $minute  = lookup("${mysystype}::backmin")
  $bindir  = lookup('filesystem::bindir')
  $etcdir  = lookup('filesystem::etcdir')
  $backcmd = "${bindir}/backup_zimbra.sh"

  # When nice is enabled, we build a cronjob command using that
  if $nice_enabled {
    $cron_cmd = "/usr/bin/nice --adjustment=${nice_value} ${backcmd}"
  } else {
    $cron_cmd = $backcmd
  }

  # ensure backdir exists
  file { $backdir:
  ensure => directory,
  mode   => '0755',
  owner  => 'zimbra',
  group  => 'zimbra',
  }

  # backups script template
  file { $backcmd:
  mode    => '0755',
  owner   => 'root',
  group   => 'root',
  content => template('is_puppet_base/backup/backup_zimbra.sh.erb'),
  require => File[ $bindir ],
  }

  # attributes files for the backup script

  # TODO: this is not an elegant way of working but at the time of this integration
  # there was no effort to open and retest the backup script

  file { "${etcdir}/zimbra_backup_attribs_lst.txt":
  mode    => '0644',
  owner   => 'root',
  group   => 'root',
  source  => 'puppet:///modules/is_puppet_base/zimbra/zimbra_backup_attribs_lst.txt',
  require => File[ $etcdir ],
  }

  file { "${etcdir}/zimbra_backup_ignore_lst.txt":
  mode    => '0644',
  owner   => 'root',
  group   => 'root',
  source  => 'puppet:///modules/is_puppet_base/zimbra/zimbra_backup_ignore_lst.txt',
  require => File[ $etcdir ],
  }

  # cron job for backup automation
  cron { 'cron_zimbra_backup':
  command => $cron_cmd,
  user    => root,
  hour    => $hour,
  minute  => $minute,
  require => File[ $backcmd ]
  }

}
