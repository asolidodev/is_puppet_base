### Purpose ########
# This class provides basic desirable properties of a generic VM server
class is_puppet_base::vmserver_base ( Boolean $ssh_strict = false,
                                      Array $ssh_acl = [],
                                      $ssh_port = 22,
) {
  group { 'sudo': ensure => 'present', }
  class { 'sudo': config_file_replace => false, }
  sudo::conf { 'sudo': priority => 10, content  => '%sudo   ALL=(ALL:ALL) ALL', }
  # Required by the sysmon_base class below (installs a nagios package which we
  # usually install with is_puppet_base::packages_base_el)
  package { [ 'nagios-plugins-disk', 'nagios-plugins-load' ]: ensure => present }

  # Required by the sysmon_base class below (creates the bin dir where the
  # script for nagios to run puppet is placed)
  include is_puppet_base::filesystem_base
  # Added to have the script for nagios to run puppet
  include is_puppet_base::sysmon_base
  include is_puppet_base::ssh_secure_el_legacy
  class { 'is_puppet_base::firewall_secure':
      strict                   => $ssh_strict,
      acl                      => $ssh_acl,
      ssh_port                 => $ssh_port
  }
  include is_puppet_base::firewall_ipv6_drop
  # needed for RAID Controller emails
  include is_puppet_base::postfix_smtp_node

  # for VM Servers with Cloudmin installed
  include is_puppet_base::cloudmin_scripts
}
