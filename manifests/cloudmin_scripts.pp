### Purpose ########
# This class provides scripts for systems with Cloudmin

class is_puppet_base::cloudmin_scripts {
  $localdir = lookup('filesystem::localdir')

  file { "${localdir}/bin/cloudmin-export.sh":
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/is_puppet_base/cloudmin/cloudmin-export.sh',
    require => File[ "${localdir}/bin" ],
  }

  file { "${localdir}/bin/cloudmin-import.sh":
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/is_puppet_base/cloudmin/cloudmin-import.sh',
    require => File[ "${localdir}/bin" ],
  }
}
