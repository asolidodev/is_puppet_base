### Purpose ########
# This class defines a sudoer user

### Dependencies ###
# classes: is_puppet_base::user_base
define is_puppet_base::user_sudoer( $myname= 'Dummy Dummier', $myhash= 'dummyhash', $mykey = 'dummykey', $mygrouplist = [ ], $ignorekey = false, $myppsigs = '', $myppwrapped = '', $myhome = '', String $myhomemode = '', String $user_shell = '/bin/bash') {

  $grouplist = flatten( [ $mygrouplist, [ 'sudo' ] ] )
  is_puppet_base::user_base { $title : myname => $myname, myhash => $myhash , mykey => $mykey, mygrouplist => $grouplist, ignorekey => $ignorekey, myppsigs => $myppsigs, myppwrapped => $myppwrapped, myhome => $myhome, myhomemode => $myhomemode, user_shell => $user_shell }

}
