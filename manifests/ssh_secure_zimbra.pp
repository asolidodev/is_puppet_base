### Purpose ########
# This class provides some sshd management class without breaking zimbra
class is_puppet_base::ssh_secure_zimbra (Integer $client_alive_interval = 14400) {
  file_line { 'sshd-config-client-alive-interval':
    ensure => present,
    path   => '/etc/ssh/sshd_config',
    match  => '^ClientAliveInterval',
    line   => "ClientAliveInterval ${client_alive_interval}"
  }

  file_line { 'sshd-config-client-alive-count-max':
    ensure => present,
    path   => '/etc/ssh/sshd_config',
    match  => '^ClientAliveCountMax',
    line   => 'ClientAliveCountMax 0'
  }
}
