### Purpose ########
# This class configures an EL system for Zimbra with nagios plugin monitor
class is_puppet_base::zimbra {
  $minimum_baggage = [perl-core, wget, w3m, elinks, unzip, nmap, nc, sysstat, libaio, rsync, telnet, aspell, mlocate]
  ensure_packages($minimum_baggage)

  $excess_baggage = ['postfix']
  package { $excess_baggage: ensure => purged, }

  sudo::conf { 'zmnagios': priority => 10, content  => 'nagios ALL=(zimbra) NOPASSWD:/opt/zimbra/bin/zmcontrol', }

  #nagios
  file { '/usr/lib64/nagios/plugins/check_zimbra.pl':
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/is_puppet_base/sysmon/check_zimbra.pl',
    # Using a require => File[ '/usr/lib64/nagios/plugins/' ]
    # fails because we don't have puppet managing that directory
    # as it's created upon install of nagios packages (these are managed by puppet)
    # so we write a dependency on one of those packages instead
    # (we have to use the EL package name, not the ubuntu name, see sysmon_base)
    require => [
      Package['nagios-plugins-disk'],
    ],
  }

}
