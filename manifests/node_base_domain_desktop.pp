### Purpose ########
# This class provides basic desirable properties of a domain based desktop node
class is_puppet_base::node_base_domain_desktop (
  Boolean $customize_syslog_mode = false,
  String $syslog_mode            = 'u=rw,g=r,o=',
  $apt_surface_list = "", # if empty it will default to a builtin list
) {

  class { 'is_puppet_base::packages_base': unattended_upgrades => true }
  include is_puppet_base::mcollective_node
  class { 'is_puppet_base::ssh_secure': password_authentication => true }
  include is_puppet_base::firewall_secure
  include is_puppet_base::firewall_ipv6_drop
  class { 'is_puppet_base::filesystem_base_desktop':
    customize_syslog_mode => $customize_syslog_mode,
    syslog_mode           => $syslog_mode,
  }
  class { 'is_puppet_base::filesystem_apt':
    apt_surface_list  => $apt_surface_list,
    server_mode       => false,
  }

  include ::ntp

  # desktops run puppet directly, not via sysmon
  service { 'puppet':
    ensure => running,
    enable => true,
  }

}
