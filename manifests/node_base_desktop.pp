### Purpose ########
# This class provides basic desirable properties of a generic desktop node
class is_puppet_base::node_base_desktop (
  Boolean $customize_syslog_mode          = false,
  String $syslog_mode                     = 'u=rw,g=r,o=',
  $apt_surface_list                       = "", # if empty it will default to a builtin list
  Boolean $firewall_strict_purge          = true,
  Array[String] $firewall_ignore_patterns = [],
) {

  class { 'is_puppet_base::packages_base': unattended_upgrades => true }
  include is_puppet_base::mcollective_node
  include is_puppet_base::ssh_secure
  class { 'is_puppet_base::firewall_secure':
    strict_purge => $firewall_strict_purge,
    ignore_patterns => $firewall_ignore_patterns,
  }
  include is_puppet_base::firewall_ipv6_drop_policy
  class { 'is_puppet_base::filesystem_base_desktop':
    customize_syslog_mode => $customize_syslog_mode,
    syslog_mode           => $syslog_mode,
  }
  class { 'is_puppet_base::filesystem_apt':
    apt_surface_list  => $apt_surface_list,
    server_mode       => false,
  }

  include ::ntp

  # desktops run puppet directly, not via sysmon
  service { 'puppet':
    ensure => running,
    enable => true,
  }

}
