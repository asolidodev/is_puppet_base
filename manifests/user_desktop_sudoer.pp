### Purpose ########
# This defined type defines a desktop sudoer user

### Dependencies ###
# defined type: is_puppet_base::user_base
# defined type: is_puppet_base::user_kde_lock_screen
define is_puppet_base::user_desktop_sudoer( $myname = 'Dummy Dummier', $myhash = '', $mykey = 'dummykey', $mygrouplist = [ ], $ignorekey = false, $myppsigs = '', $myppwrapped = '', $mytimeout = 5,  $mylockgrace = 5, String $myhomemode = '', String $user_shell = '/bin/bash') {

  $grouplist = flatten( [ $mygrouplist, [ $title, 'cdrom', 'dip', 'plugdev', 'lp', 'lpadmin', 'sambashare', 'adm', 'sudo' ] ] )
  is_puppet_base::user_base { $title : myname => $myname, myhash => $myhash , mykey => $mykey, mygrouplist => $grouplist, ignorekey => $ignorekey, myppsigs => $myppsigs, myppwrapped => $myppwrapped, myhomemode => $myhomemode, user_shell => $user_shell }

  is_puppet_base::user_kde_lock_screen { $title : mytimeout => $mytimeout, mylockgrace => $mylockgrace }

}
