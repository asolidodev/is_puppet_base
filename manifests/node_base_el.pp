### Purpose ########
# This class provides basic desirable properties of a generic EL node
class is_puppet_base::node_base_el ( Boolean $password_authentication = false,
                                      Boolean $ssh_strict = false,
                                      Array $ssh_acl = [],
                                      Boolean $customize_syslog_mode = false,
                                      String $syslog_mode = 'u=rw,g=r,o=',
                                      Boolean $firewall_secure_extra = false,
                                      Boolean $anti_ddos = false,
                                      String $ddos_limit = '100',
                                      String $ddos_limit_per_time_unit = '60',
                                      String $ddos_limit_time_unit = 'sec',
                                      $service_list = [],
                                      Boolean $ip_filter = false,
                                      String $country_whitelist = '',
                                      Array $ip_whitelist = [],
                                      Array $ip_blacklist = [],
                                      Boolean $internal_forward = false,
                                      $internal_forward_list = [],
                                      String $forward_iniface = '',
                                      String $good_mark = '0x413',
                                      String $bad_mark = '0x406',
                                      $ssh_port = 22,
) {

  # cheap Ubuntu/RHEL compatibility with user_sudoer (which requires both sudo and wheel)
  group { 'sudo': ensure => 'present', }
  class { 'sudo': config_file_replace => false, }
  sudo::conf { 'sudo': priority => 10, content  => '%sudo   ALL=(ALL:ALL) ALL', }

  include is_puppet_base::packages_base_el
  include is_puppet_base::sysmon_base
  include is_puppet_base::mcollective_node
  class { 'is_puppet_base::ssh_secure': password_authentication => $password_authentication }

  if ($firewall_secure_extra){
    class { 'is_puppet_base::firewall_secure_extra':
      strict                   => $ssh_strict,
      acl                      => $ssh_acl,
      anti_ddos                => $anti_ddos,
      service_list             => $service_list,
      ip_filter                => $ip_filter,
      country_whitelist        => $country_whitelist,
      ddos_limit               => $ddos_limit,
      ddos_limit_per_time_unit => $ddos_limit_per_time_unit,
      ddos_limit_time_unit     => $ddos_limit_time_unit,
      ip_whitelist             => $ip_whitelist,
      ip_blacklist             => $ip_blacklist,
      internal_forward         => $internal_forward,
      internal_forward_list    => $internal_forward_list,
      forward_iniface          => $forward_iniface,
      good_mark                => $good_mark,
      bad_mark                 => $bad_mark,
      ssh_port                 => $ssh_port,
    }
  }
  else {
    class { 'is_puppet_base::firewall_secure': strict => $ssh_strict, acl => $ssh_acl, ssh_port => $ssh_port }
  }

  include is_puppet_base::firewall_ipv6_drop
  class { 'is_puppet_base::filesystem_base':
    customize_syslog_mode => $customize_syslog_mode,
    syslog_mode           => $syslog_mode,
  }
  include is_puppet_base::filesystem_yum

  # per node definition overrides default definition
  $restricted_cmds  = lookup( [ "${clientcert}::filesystem::restricted_cmds",  'filesystem::restricted_cmds'  ] )
  $restricted_mode  = lookup( [ "${clientcert}::filesystem::restricted_mode",  'filesystem::restricted_mode'  ] )
  $restricted_group = lookup( [ "${clientcert}::filesystem::restricted_group", 'filesystem::restricted_group' ] )

  is_puppet_base::filesystem_sec { 'filesystem_sec': restricted_cmds => $restricted_cmds, restricted_group => $restricted_group , restricted_mode => $restricted_mode }

  class { '::selinux': mode => 'disabled', }

  include ::ntp

}
