### Purpose ########
# This class provides the minimum set of packages for a generic EL node
class is_puppet_base::packages_base_el {

  $base_packages_el = [ 'htop', 'lsof', 'psmisc', 'screen', 'tmux', 'vim', 'nagios-plugins-disk', 'nagios-plugins-load', 'nagios-plugins-dns', 'nagios-plugins-swap', 'wget' ]

  # stahnma-epel
  include epel

  package { [ $base_packages_el ]: ensure => present, require => Class[ 'epel' ] }

  if $facts['os']['family'] == 'redhat' and $facts['os']['release']['major'] == '7' {
    ensure_packages('iptables-services',{'ensure' => 'installed'})
    Package['iptables-services'] -> Firewall <| |>
    service { 'firewalld':
      enable => false,
    }

    service { 'iptables':
      enable => true,
    }
  }
}
