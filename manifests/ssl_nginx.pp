### Purpose ########
# class to control ssl cert and key for nginx
class is_puppet_base::ssl_nginx (
  String $sslprefix
)
{

  is_puppet_base::ssl_base { 'is_puppet_base::ssl_base_nginx':
    myprefix      => $sslprefix,
    myservice     => 'nginx',
    myservicename => 'nginx'
  }

}
