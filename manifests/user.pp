### Purpose ########
# This class defines a normal user not belonging to any special groups

### Dependencies ###
# classes: is_puppet_base::user_base
define is_puppet_base::user( $myname = 'Dummy Dummier', $myhash = '', $mykey = 'dummykey', $mygrouplist = [ ], $myhome = '', String $myhomemode = '', String $user_shell = '/bin/bash') {

  is_puppet_base::user_base { $title : myname => $myname, myhash => $myhash , mykey => $mykey, mygrouplist => $mygrouplist, myhome => $myhome, myhomemode => $myhomemode, user_shell => $user_shell }

}

