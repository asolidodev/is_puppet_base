### Purpose ########
# This class implements prerequisites for the backup_base_rsync defined type
class is_puppet_base::backup_base_rsync_pre {

  $bindir  = lookup('filesystem::bindir')

  package { [ 'rsnapshot' ]: ensure => present, }

  file { '/etc/rsnapshot.d':
    ensure => directory,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
  }

  file { "${bindir}/rdiff.sh":
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/is_puppet_base/backups/rdiff.sh',
  }

}
