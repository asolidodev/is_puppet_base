### Purpose ########
# This class implements simple rsnapshot based generic backups
define is_puppet_base::backup_base_rsync ( $basedir, $prefix, $backdir, $ndays, $bindir = '', $user = 'root', $hour = '23', $minute = '0')  {

  $myconffile = "/etc/rsnapshot.d/rsnapshot-${title}.conf"
  file { $myconffile:
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('is_puppet_base/backup/rsnapshot.conf.erb'),
  }

  cron { "cron_snapshot_${title}_backup":
    command => "/usr/bin/rsnapshot -c ${myconffile} daily",
    user    => $user,
    hour    => $hour,
    minute  => $minute,
    require => File[ $myconffile ]
  }

}
