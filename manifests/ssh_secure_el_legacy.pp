### Purpose ########
# This class provides LEGACY EL sshd management class
class is_puppet_base::ssh_secure_el_legacy(Integer $client_alive_interval = 14400) {

  case $::osfamily {
      'RedHat': {
        $ssh_service = 'sshd'
      }
      default: {
        $ssh_service = 'ssh'
      }
  }

  # our own modified moduli file
  file { '/etc/ssh/moduli' :
  owner  => root,
  group  => root,
  mode   => '0644',
  source => 'puppet:///modules/is_puppet_base/ssh/moduli',
  notify => Service[ $ssh_service ]
  }

  class { 'ssh::server':
  options => {
  'Protocol'                => '2' ,
  'HostKey'                 => [ '/etc/ssh/ssh_host_ed25519_key' , '/etc/ssh/ssh_host_rsa_key' ],
  'KexAlgorithms'           => 'diffie-hellman-group-exchange-sha256',
  'Ciphers'                 => 'aes256-ctr,aes192-ctr,aes128-ctr',
  'MACs'                    => 'hmac-sha2-512,hmac-sha2-256',
  'HostbasedAuthentication' => 'no' ,
  'IgnoreRhosts'            => 'yes' ,
  'LoginGraceTime'          => '120' ,
  'LogLevel'                => 'INFO' ,
  'PasswordAuthentication'  => 'no' ,
  'PermitEmptyPasswords'    => 'no' ,
  'PermitRootLogin'         => 'no' ,
  'Port'                    => '22' ,
  'PrintLastLog'            => 'yes' ,
  'PubkeyAuthentication'    => 'yes' ,
  'StrictModes'             => 'yes' ,
  'SyslogFacility'          => 'AUTH' ,
  'TCPKeepAlive'            => 'yes' ,
  'ClientAliveCountMax'     => '0',
  'ClientAliveInterval'     => $client_alive_interval,
  'UsePrivilegeSeparation'  => 'yes' ,
  'X11DisplayOffset'        => '10' ,

              },
  }

}
