### Purpose ########
# defined type to control ssl cert and key for nginx
define is_puppet_base::ssl_nginx_domain (
  String $sslprefix,
  Boolean $letsencrypt_certificate = false,
)
{

  is_puppet_base::ssl_base { "ssl_${sslprefix}":
    myprefix                => $sslprefix,
    myservice               => 'nginx',
    myservicename           => 'nginx',
    letsencrypt_certificate => $letsencrypt_certificate,
  }

}
