### Purpose ########
# This class provides a nagios plugin that can be used trigger and monitor integrity checks
class is_puppet_base::sysmon_integrity_node ( $checklist = '' ) {

  $bindir   = lookup('filesystem::bindir')

  # integrity, template uses the checklist variable
  file { "${bindir}/nagios-integrity.sh":
  mode    => '0755',
  owner   => 'root',
  group   => 'root',
  content => template('is_puppet_base/sysmon/nagios-integrity.sh.erb'),
  require => File[ $bindir ],
  }

}
