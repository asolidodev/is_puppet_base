### Purpose ########
# This class provides basic desirable properties of a LEGACY EL node
# LEGACY RHEL/CENTOS 6 - TO BE BURIED!
class is_puppet_base::node_base_el_legacy {

  # cheap Ubuntu/RHEL compatibility with user_sudoer (which requires both sudo and wheel)
  group { 'sudo': ensure => 'present', }
  class { 'sudo': config_file_replace => false, }
  sudo::conf { 'sudo': priority => 10, content  => '%sudo   ALL=(ALL:ALL) ALL', }

  include is_puppet_base::packages_base_el
  include is_puppet_base::sysmon_base
  include is_puppet_base::mcollective_node
  include is_puppet_base::ssh_secure_el_legacy
  include is_puppet_base::firewall_secure
  include is_puppet_base::firewall_ipv6_drop
  # Warning: this class has been refactored (see #115) but the refactor
  # was not tested in CentOS 6 as we don't seem to have one around anymore
  include is_puppet_base::filesystem_base
  include is_puppet_base::filesystem_yum

  # per node definition overrides default definition
  $restricted_cmds  = lookup( [ "${clientcert}::filesystem::restricted_cmds",  'filesystem::restricted_cmds'  ] )
  $restricted_mode  = lookup( [ "${clientcert}::filesystem::restricted_mode",  'filesystem::restricted_mode'  ] )
  $restricted_group = lookup( [ "${clientcert}::filesystem::restricted_group", 'filesystem::restricted_group' ] )

  # droppped due to ping problems on sysmon (RHEL6)
  is_puppet_base::filesystem_sec { 'filesystem_sec': restricted_cmds => $restricted_cmds, restricted_group => $restricted_group , restricted_mode => $restricted_mode }

  class { '::selinux': mode => 'disabled', }

  include ::ntp

}
