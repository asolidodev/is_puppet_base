### Purpose ########
# This class provides a Puppet based user directory and CLI management interface called Hashman

### Outputs ########
# - pp_auth.py CLI interface
# - empty usable user database after first run

### Dependencies ###
#  packages: see below
#  hiera: see below
class is_puppet_base::hashman_base {

  $localdir    = lookup('filesystem::localdir')
  $hashmandir = lookup('hashman::bindir')
  $dbdir      = lookup('hashman::dbdir')
  $company    = lookup('hashman::company')
  $url        = join ( [ 'https://' , lookup('hashman::address') ] )
  $mailfrom   = lookup('hashman::mailfrom')
  $team       = lookup('hashman::team')
  $testenv    = lookup('hashman::testenv')

  # get minpassword len and relax pass requirements vars
  $minpasswordlen = lookup({ name => 'hashman::minpasswordlen' , default_value => '6' })
  $relaxp = lookup({ name => 'hashman::relaxpassword'  , default_value => true })

  # converting from Puppet boolean to Python boolean
  if ( $relaxp == true )
  {
    $relaxpassword = 'True'
  }
  else
  {
    $relaxpassword = 'False'
  }

  $dbfiles  = [ "${dbdir}/cnames.pp" , "${dbdir}/companies.pp", "${dbdir}/emails.pp", "${dbdir}/hashes.pp", "${dbdir}/keys.pp", "${dbdir}/ppsigs.pp", "${dbdir}/ppwrapped.pp" ]

  # dir for binaries, dir for database
  file { [ $hashmandir, $dbdir ]:
  ensure => directory,
  mode   => '0644',
  owner  => 'root',
  group  => 'root',
  }

  file { [ "${hashmandir}/common", "${hashmandir}/plugins", "${hashmandir}/plugins/1", "${hashmandir}/plugins/2", "${hashmandir}/plugins/3"  ]:
  ensure  => directory,
  mode    => '0644',
  owner   => 'root',
  group   => 'root',
  require => File[ $hashmandir ],
  }

  file { "${hashmandir}/common/hashman_utils.py":
  mode    => '0755',
  owner   => 'root',
  group   => 'root',
  source  => 'puppet:///modules/is_puppet_base/hashman/common/hashman_utils.py',
  require => File[ "${hashmandir}/common" ],
  }

  file { "${hashmandir}/common/pp_auth.py":
  mode    => '0755',
  owner   => 'root',
  group   => 'root',
  content => template('is_puppet_base/hashman/pp_auth.py.erb'),
  require => File[ "${hashmandir}/common" ],
  }

  # shell wrapper to solve encoding issues #56
  file { "${localdir}/bin/pp_auth.sh":
  mode    => '0755',
  owner   => 'root',
  group   => 'root',
  content => template('is_puppet_base/hashman/pp_auth.sh.erb'),
  require => File[ "${localdir}/bin" ],
  }

  # initdb if necessary
  file { $dbfiles:
  ensure  => 'present',
  replace => 'no', # can't touch this!
  content => '',
  mode    => '0644',
  require => File[ $dbdir ],
  }

  package { [ 'python-cracklib', 'python-setproctitle', 'python-openssl', 'python-rsa' ]: ensure => present, }

}
