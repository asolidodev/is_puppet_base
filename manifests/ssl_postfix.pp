### Purpose ########
# class to control ssl cert and key for postfix
class is_puppet_base::ssl_postfix (
  String $sslprefix,
  Boolean $letsencrypt_certificate,
){
  is_puppet_base::ssl_base { 'postfix ssl config':
    myprefix                => $sslprefix,
    myservice               => 'postfix',
    myservicename           => 'postfix',
    letsencrypt_certificate => $letsencrypt_certificate,
  }
}
