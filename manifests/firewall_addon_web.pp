### Purpose ########
# This class provides COMPLEMENTARY firewall configurations for web traffic
### Warnings #######
# Do not forget to do the main firewall configuration,
# either by including one of our base classes
# (is_puppet_base::firewall or is_puppet_base::firewall_secure)
# or trough some other alternative (e.g. bash scripts).
### Dependencies ###
#  modules: puppetlabs-firewall

class is_puppet_base::firewall_addon_web {

  # webserver specific firewall rules - simple case
  firewall { '200 accept http':  proto  => 'tcp', dport  => 80,  action => 'accept', }
  firewall { '201 accept https': proto  => 'tcp', dport  => 443, action => 'accept', }

}
