### Purpose ########
# This defined type defines a Zimbra mailbox user

### Dependencies ###
# classes: is_puppet_base::user_base
define is_puppet_base::user_zimbra(
  Enum['present','absent'] $myensure = 'present',
  String $myname,
  String $myhash,
  # We cannot use undef here for the value (the type check fails because that's not a string)
  # so we use "" instead
  # beware at present this seems to create a 10GB quota when the user is a NEW user
  # (he doesn't get the default COS, and no zimbraMailQuota setting either)
  String $mysize = '',
  Boolean $is_admin = false,
) {

    if ($is_admin){
        $is_admin_string = 'true'
    }
    else {
        $is_admin_string = 'false'
    }

  # the resource title is expected to be the email address (i.e. it includes the domain)
  $myemailaddress = $title

  # assert that we have something that is an email address
  validate_email_address($myemailaddress)

  # split the email address
  $email_address_split = split($myemailaddress, '@')

  # get the username and domain from the email address elements
  $myusername = $email_address_split[0]
  $mydomain   = $email_address_split[1]

  # if the mailbox size is not defined...
  if $mysize == '' {
    # ... then don't enforce the mailbox size
    zimbra_user {
      "${myusername}@${mydomain}":
        ensure    => $myensure,
        mailbox   => $myusername,
        domain    => $mydomain,
        user_name => $myname,
        pwd       => $myhash,
        is_admin  => $is_admin_string,
    }
  } else {
    # if the mailbox size is defined, we enforce it:
    zimbra_user {
      "${myusername}@${mydomain}":
        ensure       => $myensure,
        mailbox      => $myusername,
        domain       => $mydomain,
        user_name    => $myname,
        pwd          => $myhash,
        mailbox_size => $mysize,
        is_admin     => $is_admin_string,
    }
  }

}
