#!/bin/bash

E_ERR=1

# we add pipes to the spaced separated input lists so we can easily use grep

if [ -z "$1" ]; then
  echo "usage: apt-check-CVEs.sh CVELIST [PKGLIST]"
  exit $E_ERR
else
  CVELIST=`echo "$1" | sed 's/ /|/g'`
fi

# we want to match the packagename in the beggining of a line and followed by a comma
if [ ! -z "$2" ]; then
  AUX=`echo "$2" | sed 's/ /,|^/g'`
  PKGLIST="^$AUX,"
else
  PKGLIST=
fi

# unfortunately apt list does not provide a formatted ouput
# this initial parsing was done so that we get the format
# pkgname, version installed, version available
# it may need maintenance in the future

MYLINES=
while read line; do
  pkg_name=`echo $line | awk '{ print $1 }' | awk -F/ '{ print $1 }'`
  version_to=`echo $line | awk '{ print $2}'`
  version_from=`echo $line | awk '{ print $6}'| tr -d ']'`
  line_edited=$pkg_name,$version_from,$version_to
  #echo $line_edited
  MYLINES="$MYLINES\n$line_edited"
done < <( apt list --upgradable |grep -v "Listing..." )

# we might in the future have a direct command for this
# so this variable remains as a place holder
CHECK_CMD="echo -e $MYLINES"


echo
if [ ! -z "$PKGLIST" ]; then
  echo 'Checking for packages from the given list with available updates'
  RESULT=`$CHECK_CMD |egrep "$PKGLIST"`
else
  echo 'Checking for any packages with available updates'
  RESULT=`$CHECK_CMD`
fi

have_update=

for line in $RESULT; do
  pkg_name=`echo $line | awk -F ',' '{ print $1 }'`
  have_update="$have_update $pkg_name"
done

echo
echo 'Packages that have updates are:'
echo $have_update
echo

# incremental variable for acumulating matching packages
to_update=

for line in $RESULT; do
  pkg_name=`echo $line | awk -F ',' '{ print $1 }'`
  version_installed=`echo $line | awk -F ',' '{ print $2 }'`
  version_candidate=`echo $line | awk -F ',' '{ print $3 }'`
  # show changelog only until it reaches the installed version
  echo "Checking interesting CVEs for package $pkg_name"
  apt-get changelog $pkg_name=$version_candidate 2>/dev/null |sed -n  "/$version_installed/q;p" | egrep $CVELIST
  RC=$?
  if [ $RC -eq 0 ]; then
    to_update="$to_update $pkg_name"
  fi
done

echo
echo "List of packages that contain fixes for monitored CVEs:"
echo $to_update
