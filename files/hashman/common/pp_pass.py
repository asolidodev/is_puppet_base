#!/usr/bin/python

import sys
import os
import setproctitle
import logging
import logging.handlers
from pp_auth import *

# definitions
PASSWD_FILE = '/etc/puppet/manifests/passwords.pp'
OURPROCTITLE = 'pp_pass.py'

# basic error codes for functions called from main()
E_OK = 0
E_ERR = 1

# command line invocation summary
def print_usage():
    basename = os.path.basename(sys.argv[0])
    print 'Usage: ' + basename + ' USER PASSWORD'
    exit (E_ERR)

def main():

    # mask credentials on cmdline, first things first
    setproctitle.setproctitle(OURPROCTITLE)

    # normal use: INFO, debug use: DEBUG
    logging.basicConfig(level=logging.INFO)

    cmd = ''
    extramsg = ''
    nargs = len(sys.argv)
    # argument 0 also counts
    if nargs < 2 :
        print_usage()
    else:
        username = sys.argv[1]
        password = mkpass( PASSWORDLEN )
        rc = setpw ( username , password, True, PASSWD_FILE )
        sys.exit(rc)

# we may start
if __name__ == "__main__":
    main()
