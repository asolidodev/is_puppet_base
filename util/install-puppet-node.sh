#!/bin/bash
# usage example: sudo ./install-puppet-node.sh FQDN DOMAIN MASTER CUSTOMER

# MASTER
## puppet.angulosolido.pt
## puppet-staging.angulosolido.pt

# CUSTOMER list
## future-healthcare
## angulosolido

RELEASE_CHECK=`lsb_release -cs`

# because there is yet no focal nor jammy puppet packages
# we install the bionic version that seems to work so far
if [ $RELEASE_CHECK == 'focal' ] ||  [ $RELEASE_CHECK == 'jammy' ];then
    RELEASE='bionic'
else
    RELEASE=`lsb_release -cs`
fi

SETHOSTNAME=yes
START=no
REPOPKG=/tmp/puppetlabs-release-$RELEASE.deb
REPOPKGURL=http://apt.puppetlabs.com/puppet5-release-$RELEASE.deb
NODENAME=$1
DOMAIN=$2
NODECONF=/etc/puppetlabs/puppet/puppet.conf
PUPPETBIN=/opt/puppetlabs/bin/puppet
MASTER=$3
CUSTOMER=$4

#### main ####

if [ -z $NODENAME ]; then
 echo usage: $0 NODENAME DOMAIN [MASTER]
 exit
fi

if [ -z $DOMAIN ]; then
 echo usage: $0 NODENAME DOMAIN [MASTER]
 exit
fi

if [ -z $MASTER ]; then
 MASTER=puppet.$DOMAIN
fi

# set hostname
if [ x$SETHOSTNAME = 'xyes' ]; then
 hostnamectl set-hostname  $NODENAME
 grep $NODENAME /etc/hosts || echo "127.0.0.1 ${NODENAME}.${DOMAIN}" ${NODENAME} >> /etc/hosts
fi

apt-get update
apt-get install -y wget

wget -O $REPOPKG $REPOPKGURL
dpkg -i $REPOPKG

apt-get update
apt-get install -y ntp 
apt-get install -y puppet-agent

# configurations  and start puppet service
cp -f $NODECONF ${NODECONF}.orig
echo -e "[main]\nserver=${MASTER}\nnode_name=cert\ncertname=${NODENAME}\nlogdir=/var/log/puppet\nvardir=/var/lib/puppet\nssldir=/var/lib/puppet/ssl\nrundir=/var/run/puppet\nfactpath=$vardir/lib/facter" > /etc/puppetlabs/puppet/puppet.conf

$PUPPETBIN resource service puppet ensure=running enable=true

if [ x$START = 'xyes' ]; then
  systemctl enable puppet.service
  systemctl start puppet.service
else
  systemctl disable puppet.service
  systemctl stop puppet.service
fi

# custom customer fact
mkdir -p /etc/facter/facts.d
echo "customer=$CUSTOMER" > /etc/facter/facts.d/facts.txt

$PUPPETBIN agent --test
